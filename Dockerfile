FROM python:3.8.10-alpine

ENV PYTHONUNBUFFERED 1

COPY tornado_test/../ /tornado_test
WORKDIR /tornado_test

RUN apk update && apk upgrade
RUN pip install --upgrade pip
RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
    build-base gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev libffi-dev libxml2-dev libxslt-dev
RUN pip install -r requirements.txt
