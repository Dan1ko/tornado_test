from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from tornado.web import RequestHandler
from config import db_url, PROD
from models import Array
import sqlalchemy
import json


database_url = db_url(PROD)

engine = create_async_engine(database_url, echo=True)

async_session = sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)


class MainHandler(RequestHandler):

    async def get(self):
        self.write('Hello, world!')


class ArrayHandler(RequestHandler):

    async def get(self):
        request_data = json.loads(self.request.body)
        response_body = None
        if 'id' in request_data.keys() and type(request_data.get('id')) == int:
            id_of_array = request_data['id']
            if id_of_array:
                try:
                    async with async_session() as session:
                        get_array_query = (sqlalchemy.select(Array).where(Array.id == id_of_array))
                        array = await session.execute(get_array_query)
                        if array:
                            new_array = array.fetchone()
                            object = [i for i in new_array][0]
                            response_body = {
                                "result": "OK", "id": object.id,
                                "array": object.array, "result_array": object.result_array,
                                "date_of_creation": object.date_of_creation.strftime("%d-%m-%YT%H:%M:%S")
                            }
                        else:
                            response_body = {
                                "result": "ERROR"
                            }

                except Exception as e:
                    self.write(f'Exception in ArrayHandler.get() : {e}')
            else:
                response_body = {
                    "result": "ERROR"
                }
        else:
            response_body = {
                "result": "ERROR"
            }
        self.write(response_body)

    async def post(self):
        request_data = json.loads(self.request.body)
        response_body = None
        if 'array' in request_data.keys() and type(request_data.get('array')) == list:
            array = request_data["array"]
            if array:
                result_array = sorted(array)
                try:
                    async with async_session() as session:
                        data_to_write = (
                            sqlalchemy.insert(Array).values(array=array, result_array=result_array).returning(Array)
                        )
                        array = await session.execute(data_to_write)
                        await session.commit()
                        if array:
                            new_array = array.fetchone()
                            response_body = {
                                "result": "OK", "id": new_array[0],
                            }
                        else:
                            response_body = {
                                "result": "ERROR"
                            }

                except Exception as e:
                    self.write(f'Exception in ArrayHandler.post{e}')
            else:
                response_body = {
                    "result": "ERROR"
                }
        else:
            response_body = {
                "result": "ERROR"
            }
        self.write(response_body)
