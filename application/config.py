import os

DATABASE_URL = "postgresql+asyncpg://tornado:tornado@localhost:5432/tornado"
PROD = True
DB_USER = os.environ.get("DB_USER")
DB_PASSWORD = os.environ.get("DB_PASSWORD")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = os.environ.get("DB_PORT")
DB_NAME = os.environ.get("DB_NAME")


def db_url(status):
    if status:
        database_url = f'postgresql+asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
    else:
        database_url = DATABASE_URL
    return database_url
