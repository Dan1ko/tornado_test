from handlers import MainHandler, ArrayHandler
from tornado.options import define, options
from tornado_sqlalchemy import SQLAlchemy
from tornado.web import Application
from tornado.ioloop import IOLoop
from config import db_url, PROD

database_url = db_url(PROD)

define("port", default=8888, help="run on the given port", type=int)
define("debug", default=True, help="run in debug mode")


def make_app():
    routes = [
        (r"/", MainHandler),
        (r"/api/array/", ArrayHandler),
    ]
    return Application(routes, db=SQLAlchemy(database_url), debug=options.debug)


if __name__ == '__main__':
    app = make_app()
    app.listen(options.port)
    IOLoop.instance().start()
