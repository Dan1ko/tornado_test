from sqlalchemy import Column,  Integer, ARRAY, DateTime, func
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class Array(Base):
    __tablename__ = "array"

    id = Column('id', Integer, primary_key=True)
    array = Column('array', ARRAY(Integer))
    result_array = Column('result_array', ARRAY(Integer))
    date_of_creation = Column(DateTime, server_default=func.now())

    __mapper_args__ = {"eager_defaults": True}
