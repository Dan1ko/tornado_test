#!/bin/bash

alembic revision --autogenerate -m 'init' &&
alembic upgrade head &&
python3 ./application/app.py